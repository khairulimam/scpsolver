#header="/Developer/SDKs/MacOSX10.6.sdk/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/Headers/"
jniheader="/System/Library/Frameworks/JavaVM.framework/Headers/"
glpkheader="./src/glpk-4.44/include/"
gcc -fPIC -c -I$jniheader -I$glpkheader  lib/glpk_jni.c
gcc --shared -W1 -I$jniheader -I$glpkheader  \
-L/usr/local/lib/ glpk_jni.o  lib/osx/libglpk.a lib/osx/libltdl.a \
lib/osx/liblz.a  -lm -ldl -o libglpk.jnilib

#gcc -dynamiclib  -W1 -I$jniheader -I$glpkheader  glpk_jni.o lib/osx/libglpk.a  -lm -lz -lltdl -ldl -o libglpk.jnilib
#cp libglpkjni.so ~/Desktop/SCPSolver/lib/
