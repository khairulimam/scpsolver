# Build file for a static GLPK-library on a linux 32-bit machine
# Please read the instructions of the INSTALL file first
# Author: Michael Schober
# Date: 10/07/21

cd lib/
# Compiling files
gcc -fPIC -c -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ \
-I/usr/local/include/ glpk_jni.c
gcc --shared -W1 -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ \
-L/usr/local/lib/ glpk_jni.o ux_32/libglpk.a ux_32/libltdl.a \
ux_32/libz.a -lm -ldl -o libglpkjni.so
# Cleaning up
mv libglpkjni.so ../build/
rm glpk_jni.o
