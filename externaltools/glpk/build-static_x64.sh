# Build file for a static GLPK-library on a linux 32-bit machine
# Please read the instructions of the INSTALL file first
# Author: Michael Schober
# Date: 10/07/21

cd lib/
# Compiling files
gcc -fPIC -c -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ \
-I/usr/local/include/ glpk_jni.c
gcc --shared -W1 -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/ \
-L/usr/local/lib/ glpk_jni.o ux_64/libglpk.a ux_64/libltdl.a \
ux_64/libz.a -lm -ldl -o libglpkjni_x64.so
# Cleaning up
mv libglpkjni_x64.so ../build/
rm glpk_jni.o
