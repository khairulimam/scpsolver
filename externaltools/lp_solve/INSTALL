INSTALLATION GUIDE

Author: Michael Schober - micha-schober@web.de
Date: 10/07/29

This installation guide will help you set up a new version of the
lp_solve library after a new release. This guide will not consider
new versions of depending libraries, because this would also make
a much more complex management necessary. If depending libraries are
considered, we assume that you are a more experienced user who can
abstract over the here provided simpler instruction and build-files.

Additionally, this installation guide assumes that you have a working
bash installation and coreutils available. (Windows users might want
to check out MinGW & MSYS, just google for it.)

1.) Download the latest version of lp_solve source and the lp_solve
Java interface:
	- http://sourceforge.net/projects/lpsolve/
2.) Remove any prior resources from the src/ directory and move (or
copy) the files there:
	rm -rf externaltools/lp_solve/src/*
	cp [pathTo]/lp_solve_[version]_source.tar.gz \
	[pathTo]/lp_solve_[version]_java.zip externaltools/lp_solve/src/
3.) Unpack and build the new lp_solve base library
	cd externaltools/lp_solve/src
	tar xzf lp_solve_[version]_source.tar.gz
	cd lp_solve_[version]/lpsolve55/
	sh ccc

NOTE: lp_solve does not use a standard folder system for its
distribution. The above listed commands might change in new releases
of lp_solve, but are probably similar in following releases. In any
case, you might want to read the corresponding readme.txt-files in
the directories and look manually which directory builds the library.

4.) Unpack the new lp_solve Java interface:
	cd ../../
	unzip lp_solve_[version]_java.zip
5.) Run the corresponding build-script
	sh build-static[yourOsNameModifier]

NOTE: If problems occur, check your various path variables first,
e.g. PATH and JDK_HOME

6.) Copy the compiled library in the lib/-directory of your SCPSolver
installation:
	cp build/[yourOSlibraryName] [pathToYourApplicationsLibraryFiles]

Et voila, you should be good to go. With the next application start,
lp_solve will use the new library.
