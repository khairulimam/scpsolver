package test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scpsolver.problems.LinearProgram;

public class LinearProgramTest extends TestCase  {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEvaluate() {
		LinearProgram lp = new LinearProgram(new double[]{1.0,1.0});
		double result = lp.evaluate(new double[]{1.0,1.0});
		Assert.assertEquals("Evaluation is wrong: ",2.0, result);
		result = lp.evaluate(new double[]{0.0,0.0});
		Assert.assertEquals("Evaluation is wrong: ",0.0, result);
	}



}
