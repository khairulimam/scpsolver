\documentclass{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[english]{babel}

\usepackage{hyperref}
% Bei der Erstellung dieses Files muss pdflatex verwendet werden!
\usepackage{graphicx}
\usepackage{float}

\title{Writing a solver plugin for the SCPSolver framework}
\author{Hannes Planatscher \and Michael Schober}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}
The SCPSolver library is a Java-framework for solving various
optimization problems with current focus on linear and mixed integer programming.
Although the library does not implement an own linear programing solver, it offers
a common interface for existing solvers, thus hiding the complexity of an individual
solver implementation for authors of linear optimization problems. This makes it
very easy to try different solvers on different problems or to update solver
compatibility with version changes.

On the other hand, a common interface for linear program solvers makes it easy for
solver programmers to provide the users with new implementations. All that is needed
is one wrapper class that translates between the interface requirements and the
underlying implementation structure.

An additional feature of the SCPSolver library is that it is capable of automatically
detecting available solvers in the classpath and providing the user dynamically with
requested solvers. Solvers thus can be distributed in individual modules. If an user
wants to try another solver, all he has to do is to download another solver-module and
insert it into the classpath. Solver-programmers can distribute solvers in a single
module containing all the necessary class-files and libraries.

This document explains how a solver-programmer can build a solver-module for the SCPSolver
framework.

\section{How the solver-module system works}
All linear program solvers in the SCPSolver library share a common interface \texttt{LinearProgramSolver}.
After creating a new linear optimization problem, an user usually simply requests a new solver
from the solver factory and uses this solver to compute the solution. So a common call would look
like this:

\begin{verbatim}
    LinearProgram lp;
    ...
    LinearProgramSolver solver = SolverFactory.newDefault();
    solver.solve(lp);
    // Do something with the result
\end{verbatim}

An alternative way could be that the user specifically asks for a certain solver:

\begin{verbatim}
    LinearProgramSolver solver = SolverFactory.getSolver(``GLPK'');
\end{verbatim}

The \texttt{SolverFactory} uses the
\href{http://java.sun.com/j2se/1.5.0/docs/guide/jar/jar.html#Service%20Provider}{Service Provider Interface}
to find and load available solver classes. Additionally, the \texttt{SolverFactory}
ensures that native libraries required by the requested solvers are there on runtime
and unzips the libraries from the jar-file, if necessary.

Therefore, a solver programmer only has to compile a Jar-file including
\begin{enumerate}
\item all class files needed by the implementation \textbf{and}
\item all native libraries needed by the implementation (maybe depending on OS and architecture).
\end{enumerate}

\section{Specifications}
The Java Service Provider Interface requires a parameter-less constructor
for any implementing class. Therefore, if your solver is called \emph{MyFancySolver},
your class file should look something like this:

\begin{verbatim}
public class MyFancySolver implements LinearProgramSolver {
    ...
    public MyFancySolver() {
        ....
    }
    ...
}
\end{verbatim}

Since the \texttt{SolverFactory} has to have the possibility to request some
additional information before it can guarantee to fulfill all other library
dependencies of the solver, it is necessary that the solver does not invoke any native
library calls in the following methods:
\begin{itemize}
\item the standard constructor from above
\item the \texttt{getName}-method
\item the \texttt{getLibraryNames}-method
\end{itemize}
In particular, the solver must not try to load the libraries on its own, since an user
might not have unpacked the libraries to the right directories yet.

The \texttt{getLibraryNames}-method must return a String-array containing
the names of all native libraries the solver might call. If the solver
calls no native library at all, this method must return \texttt{null}. If libraries
depend on other libraries, the String-array should contain the libary names
from least depended to most depended. For example, if your solver needs the libraries
\texttt{foo}, \texttt{bar}, \texttt{baz} and \texttt{foobar}, where \texttt{foobar}
depends on \texttt{foo} and \texttt{bar} while \texttt{bar} depends on \texttt{baz}, the
\texttt{getLibraryNames} should look like this:

\begin{verbatim}
public String[] getLibraryNames () {
    return new String[]{"baz", "foo", "bar", "foobar"};
}
\end{verbatim}

Although SCPSolver supports multiple library dependencies, it is recommended to put all
required native code in one library. To achieve this, you can use tools like ldd under Linux
or \href{http://www.dependencywalker.com/}{Dependency Walker} under Windows to detect
your library dependencies and add the \emph{static} libraries to your compiler or linker.

After loading a solver class, the \texttt{SolverFactory} immediately tries
to load all libraries specified by \texttt{getLibraryNames}. If these
libraries cannot be found in the library path, the \texttt{SolverFactory}
will try to find the libraries in the jar-files in the classpath. Therefore,
it is recommmended that solver programmers include libraries for all
supported operating systems and CPU architectures in the the same jar-file
with the solver.

The libraries must follow this naming convention:
\begin{itemize}
\item the library follows the usual naming convention of the operating system (i.e. the library \texttt{foo}
would be called \texttt{foo.dll} on a Windows machine and \texttt{libfoo.so} on a Linux machine)

\item Depending on the CPU architecture, the name must be appended before the
file extension according the following table:
\begin{table}[H]
\centering
\begin{tabular}{ll}
\textbf{Architecture} & \textbf{Name modifier}\\
\hline
Intel x86 & \emph{none}\\
AMD 64 & \texttt{\textunderscore{}x64}\\ 
\end{tabular}
\caption{CPU architecture types and library name modifiers}
\end{table}

\end{itemize}

However, there is no restriction concerning the directory path inside of the jar-file.
A common convention is to include all libraries in a directory \texttt{lib}.

To make a solver module out of an existing jar file, you'll have to add an extra
file in the \texttt{META-INF/services/} directory with the name \texttt{scpsolver.lpsolver.LinearProgramSolver} which contains the package- and class-name
of your solver. So, if your solver is called \texttt{MyFancySolver} and is in
the package \texttt{org.fancy.solvers}, then the file would contain a line
\texttt{org.fancy.solvers.MyFancySolver}.

\section{Example: how we build the GLPK solver interface}
The first solver plugin we build was a Java portation of the
\href{http://www.gnu.org/software/glpk/}{GNU Linear Programming Kit}. This section
describes how we build the interface and on which difficulties we stumbled upon.

Our task was simplified by the fact, the we could already make use of
\href{http://bjoern.dapnet.de/glpk/}{an existing portation} available on the internet.
Subsequently, we only had to write a wrapper class that translates between the SCPSolver
interfaces and the GLPK code. Additionally, we deleted the lines in the source code in
which the native library is loaded, since that is done by the SolverFactory.

However, we recompiled the native library to eliminate unnecessary other library dependencies.

Under Linux we simply downloaded the sources from the corresponding websites and build
it from scratch. The GLPK solver compiled without issues. For compiling the final
library, we added the static versions of the required libraries to the \texttt{gcc} to
the command line.

Under Windows, things were a little bit more complicated. We installed \href{http://www.mingw.org/}{MinGW}, but we couldn't build GLPK successfully with that, so we had to additionally
install \href{http://www.microsoft.com/germany/express/product/visualcplusplusexpress.aspx}{Microsoft Visual C++ 2008 Express Edition}. Using the batch-script from the \texttt{w32/}
directory, we could build the GLPK libraries. We downloaded the other static libraries
from the \href{http://gnuwin32.sourceforge.net/}{GnuWin32-homepage} and final build our
dynamic library with MinGW with the same compiler command line to the Linux version, except
for an additional linker command which prohibited the usage of name mangling techniques of the Windows plattform.

\section{Resources}
This section includes links to mentioned programs and other manuals and helpful sites on the Internet.
\begin{itemize}
\item \emph{The Java Service Provider Interface:}
\begin{itemize}
\item \href{http://java.sun.com/j2se/1.5.0/docs/guide/jar/jar.html#Service%20Provider}{Official Specifications}
\item \href{http://java.sun.com/developer/technicalArticles/javase/extensible/index.html}{Detailed tutorial}
\end{itemize}

\item \emph{The GNU Linear Programming Kit}
\begin{itemize}
\item \href{http://www.gnu.org/software/glpk/}{Official GLPK homepage}
\item \href{http://bjoern.dapnet.de/glpk/}{GLPK JNI interface}
\end{itemize}

\item \emph{Additional compilation programs under Windows}
\begin{itemize}
\item \href{http://www.microsoft.com/germany/express/product/visualcplusplusexpress.aspx}{Microsoft Visual C++ 2008 Express Edition}
\item \href{http://www.mingw.org/}{MinGW} -- for additional compilation
\item \href{http://gnuwin32.sourceforge.net/}{GnuWin32} -- Windows portations of GNU programs (for libraries)
\item \href{http://www.dependencywalker.com/}{Dependency Walker} -- for checking library dependencies under Windows
\item \href{http://www.velocityreviews.com/forums/t143642-jni-unsatisfied-link-error-but-the-method-name-is-correct.html}{forum entry discussing JNI and names mangling}
\end{itemize}
\end{itemize}

\end{document}
